Veterans Day � November 11
[Veterans Day](https://en.wikipedia.org/wiki/Veterans_Day) is a day to celebrate for veterans and honor them for what they have done for the country. It is a federal holiday in United States and it is a one of the non-working day in November. Sometimes, Memorial Day (which is in May) and Veterans Day can be conflicted by society but in Memorial Day, people honor people who have died while serving in military service of United States. But in Veterans Day, people honor the veterans who survived from the World War I, World War II and from the other wars all around the world that United States participated.

Source Image: https://www.123calendars.com/images/2019/November/November-2019-Calendar.jpg

In this day both government and citizens of United States celebrate and honor veterans. So you might participate these celebrates and events. But in case if you are not going to be a part of these events across the country, you should consider that, you might see some crowded areas and you should organize your plans according to these celebrations. If you know someone who is a veteran, you definitely should honor them by giving a thanks speech or speak with them on a normal daily things. Since it is a federal holiday across country, you might spend some time with your family as well. That is the biggest reason why you should mark this day on your calendar.

Source: https://www.123calendars.com/november-calendar.html
